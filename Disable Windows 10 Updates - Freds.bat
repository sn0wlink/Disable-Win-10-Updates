

:: Windows 10 Update Killer
:: Teal Press Ltd
:: David Collins-Cubitt
:: October 2016

:: warning message
msg * "Make sure you run this as administrator"

:: Disables service
sc config wuauserv start= disabled

:: completion message
msg * "Windows 10 Update Has Been Disabled"